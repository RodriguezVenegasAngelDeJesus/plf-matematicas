# 1- Conjuntos, Aplicaciones y funciones (2002)

```plantuml
@startmindmap
<style>
mindmapDiagram {
    node {
        BackgroundColor lightGreen
    }
    .white {
        BackgroundColor white
    }
    boxless {
        FontColor darkgreen
    }
}
</style>
* Matemáticas
** Conjuntos <<white>>
***_ definición
**** Una colección abstracta de elementos en donde puede identificarse un rasgo en común
***_ conceptos primitivos
**** Inclusión de conjuntos <<white>>
*****_ Se dice que
****** Un conjunto está contenido en otro, si todo elemento del primero pertenece a los elementos del segundo conjunto
**** Operaciones básicas <<white>>
***** Intersección <<white>>
******_ es
******* Elementos que pertenecen simultáneamente en ambos conjuntos
***** Unión <<white>>
******_ es
******* Elementos que pertenece al menos alguno de ellos
***** Complementación <<white>>
******_ es
******* No pertenece a un conjunto dado
***_ tipos
**** Universal <<white>>
*****_ es
****** Un conjunto de referencial en el que ocurre todas las cosas de la teoría
**** Vacío <<white>>
*****_ es 
****** No tiene ningún elemento
***_ representación gráfica
**** Diagrama de Venn <<white>>
*****_ propuesto por
****** Jhon Venn
*****_ características
****** Emplean figuras para delimitar a cada uno de los elementos
****** Cuentan con intersecciones para visualizar las semejanzas que hay entre los elemntos
****** Representan de forma gráfica la relación lógica que existe entre los elementos
*** Cardinal de un conjunto <<white>>
****_ es
***** El número de elementos que tiene dichos conjuntos
** Aplicaciones <<white>>
***_ definición
**** Se define como aplicación entre dos conjuntos cuales quiera a la operación que relaciona elementos del conjunto de salida con el del conjunto de llegada.
***_ elementos
**** Dominio <<white>>
*****_ es
****** Un subconjunto del conjunto de salida
**** Codominio <<white>>
*****_ es
****** Un subconjunto de conjunto de llegada de la aplicación entre dos conjuntos
** Funciones <<white>>
***_ definición
**** El conjunto de salida es igual al conjunto de definición
***_ tipos
**** Inyectiva <<white>>
***** Se dice que una funcion es inyectiva si para todo elemento de un conjunto es igual al elemento de otro conjunto
**** Sobreyectiva <<white>>
***** Es decir todo elemento del conjunto de salida A  tiene su correspondiente imagen en B 
**** Biyectiva <<white>>
***** Una función es biyectiva o biunívoca si es inyectiva y sobreyectiva
@endtmindmap
```

# 2- Funciones (2010)

```plantuml
@startmindmap
<style>
mindmapDiagram {
    node {
        BackgroundColor lightGreen
    }
    .white {
        BackgroundColor white
    }
    boxless {
        FontColor darkgreen
    }
}
</style>
* Fuciones
**_ definición en\n matemática
*** Es una relación que se establece entre dos conjuntos
*** Cada elemento del primer conjunto se le asigna un único elemento del segundo o ninguno
**_ representación gráfica
*** Representación Cartesiana <<white>>
****_ es 
***** Es una representaación gráfica de una relación matemática
***** Son un tipo de coordenadas ortogonales usadas en espacios euclídeos
****_ en honor a
***** René Descartes
**_ características
*** Función Crecientes <<white>>
****_ es
***** Cuando aumenta la variable independiente del primer conjunto,\n los valores del otro conjunto aumentan sus imágenes
*** Función Decrecientes <<white>>
****_ es
***** Cuando disminuyen la variable independiente del primer conjunto,\n los valores del otro conjunto disminuyen
*** Intervalo <<white>>
****_ es
***** Un trozo del rango de los valores que puede tomar
*** Máximo Relativo <<white>>
**** Es el punto con el valor más alto en un segmento
*** Mínimo Relativo <<white>>
**** Es el punto con el valor más pequeño en un segmento
*** Función Continua <<white>>
****_ es
***** Son las que no presentan ningún punto aislado, saltos o interrupciones
*** Función Discuntinua <<white>>
****_ es
***** Son lasque presentan un punto aislado, saltos o interrupciones
**_ que hace
*** Límite <<white>>
****_ es
***** Como se aproxima la variable x a los valores de una función
*** Derivada <<white>>
****_ es 
***** La derivada lo que hace es resolver el problema de la aproximación de una función compleja
@endtmindmap
```

# 3- La matemática del computador (2002)

```plantuml
@startmindmap
<style>
mindmapDiagram {
    node {
        BackgroundColor lightGreen
    }
    .white {
        BackgroundColor white
    }
    boxless {
        FontColor darkgreen
    }
}
</style>
* Matemáticas del Computador
** Matemática Básica <<white>>
*** Números Reales <<white>>
****_ es
***** Números abstractos teóricos de la extacción del número real
****_ ejemplo
***** raiz de 2
*** Números Fraccionarios <<white>>
****_ es
***** Se expresan de las forma a/b o como una expresión decimal periódicas
*** Mantisa <<white>>
****_ es
***** La mantisa de un numero decimal es su parte decimal o fraccionaria
** Aritmética Finita <<white>>
*** Truncamiento <<white>>
****_ consiste
***** En suprimir todos los digitos que existen tras el ultimo representable
*** Redondeo <<white>>
****_ consiste
***** Es el resultado de reemplazar un número por su forma de punto flotante, es decir por su representación en una maquina concreta
*** Desbordamiento <<white>>
****_ es
***** Cuando una operación aritmética intenta crear un valor numérico que está fuera del rango que puede representar
** Sistemas Numéricos <<white>>
*** Decimal <<white>>
****_ es
***** Sistema de numeración posicional en el que las cantidades se representan utilizando como base el número diez
****_ como se representan
***** Se compone de diez cifras diferentes del 0 al 9
*** Binario <<white>>
****_ es
***** Sistema de numerción en el que los números se representan utilizando solamente las cifras 0 y 1
****_ como se representan
***** Codificación binaria en base a la representacion 0 la ausencia de corriente y 1 la presencia de corriente
****_ puede representar
***** Letras
***** Números
***** Signos
*** Octal <<white>>
****_ es
***** Sistema de numeración en base 8
****_ como se representan
***** Utiliza los dígitos del 0 al 7 
*** Hexadecimal <<white>>
****_ es
***** Sistema de numeración posicional de base 16
****_ como se representan
***** Utiliza los digitos del 0 al 9 y las primeras letras del alfabeto latino (A,B,C,D,E,F)
@endtmindmap
```